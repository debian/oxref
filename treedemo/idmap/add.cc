#include "idmap.ih"

void IDmap::add(string const &name, initializer_list<string> &&calls)
{
    for (auto begin = calls.begin(), end = calls.end(); begin != end; ++begin)
        this->insert( pair{ *begin, vector<string>{} } );

    (*this)[name] = std::move(calls) ;
    
    //insert( pair{ name, std::move(calls) } );
}


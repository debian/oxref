#include "main.ih"

// Room for Args initialization

int main(int argc, char **argv)
try
{
    IDmap idMap;
    idMap.add("A", { "B", "C", "D", "A"});
    idMap.add("B", { "E", "F" });
    idMap.add("C", { "A" });
    idMap.add("D", { "P", "Q" });
    idMap.add("F", { "R", "S", "B" });
    idMap.add("Q", { "K", "L" });

    Tree tree{ idMap };

    tree.print("A");

    cout << "\n"
            "-----------------\n"
            "\n";

    tree.print("B");

}
catch (...)
{
    return 1;
}

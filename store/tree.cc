#include "store.ih"

void Store::tree(string const &symbol) const
{   
    size_t idx = symbolIdx(symbol);

    if (idx == d_xrefVector.size())
    {
        cout << '`' << symbol << "' not found\n";
        return;
    }

    cout << setfill('-') << setw(70) << '-' << setfill(' ') << "\n"
            "CALL TREE FOR: " << symbol << "\n"
            "\n";

    Tree tree{ d_xrefVector };

    tree.print(idx);            // print the tree, starting at symbol idx
}


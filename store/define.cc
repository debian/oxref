#include "store.ih"

void Store::define(std::string const &symbol, bool isFunction)
{
    auto iter = find_if(
                    d_xrefVector.begin(), d_xrefVector.end(),
                    [&](XrefData const &data)
                    {
                        return data.isDefined(symbol);
                    }
                );

    if (iter != d_xrefVector.end())
        return;                         // symbol already defined

    iter = find_if(
                d_xrefVector.begin(), d_xrefVector.end(),
                [&](XrefData const &xrefData)
                {
                    return xrefData.hasSymbol(symbol);
                }
            );

    size_t currentIdx;

    if (iter != d_xrefVector.end())
    {
        currentIdx = iter - d_xrefVector.begin();
        d_xrefVector[currentIdx].setLocation(d_sourceFile, d_objFile);
        d_defIdx.push_back(currentIdx);
    }
    else
    {

                            // new symbol: defined at index currentIdx, 
                            // any *UND* elements are for function currentIdx
                            // if not a function, then d_currentIdx isn't 
                            // used with *UND* elements
        d_defIdx.push_back(currentIdx = d_xrefVector.size());

        d_xrefVector.push_back(
                XrefData(d_sourceFile, d_objFile, isFunction, symbol)
        );
    }

    if (isFunction)
        d_currentIdx = currentIdx;
}



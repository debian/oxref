#include "store.ih"

void Store::callers(unordered_set<size_t> &done, queue<size_t> &todo) const
{
//    vector<string> calls;


    while (not todo.empty())
    {
        size_t idx = todo.front();          // get/remove the next entry idx
    
        todo.pop();
    
        if (done.find(idx) != done.end())   // entry 'idx' already processed.
            continue;

        done.insert(idx);                       // avoid repeated lists
    
        XrefData const &data = d_xrefVector[idx];       // the current entry 
        vector<size_t> const &usedBy = data.usedBy();   // and its callers
    
        if (usedBy.empty())
        {                                                   // ignore GLOBALS 
            if (data.fullName().find("GLOBALS ") == string::npos)   // entries
                cout << data.fullName() << 
                    " (" << d_xrefVector[idx].sourceFile() << 
                    "): not called\n\n";

                //calls.push_back(data.fullName() + " (" + 
                //                d_xrefVector[idx].sourceFile() +
                //                "): not called\n\n");
            continue;
        }

        //calls.push_back(data.fullName() + 
        //                " (" + d_xrefVector[idx].sourceFile() + 
        //                ") : called by\n");

        cout << data.fullName() << 
                " (" << d_xrefVector[idx].sourceFile() << "): called by\n";
    
        for (size_t idx: usedBy)
        {
            if (idx == d_globals.first)         // avoid using the GLOBALS
                idx = d_globals.second;         // reference

            if (done.find(idx) == done.end())
            {
                if (not d_xrefVector[idx].usedBy().empty())
                    todo.push(idx);
            }

            cout << "   " << ' ' << d_xrefVector[idx].fullName() << 
                    " (" << d_xrefVector[idx].sourceFile() << ")\n";

            //calls.back() += "   " + d_xrefVector[idx].fullName() +
            //                " (" +  d_xrefVector[idx].sourceFile() + ")\n";
        }

        cout.put('\n');
        //calls.back() += '\n';
    }
    
    //sort(calls.begin() + 1, calls.end());
    //for (string const &str: calls)
    //    cout << str;
}




